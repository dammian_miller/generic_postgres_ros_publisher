#ifndef PGCONNECTION_H
#define PGCONNECTION_H

#include <memory>
#include <mutex>
#include <libpq-fe.h>

class PGConnection
{
public:
    PGConnection(char conninfo_[]):conninfo(conninfo_)
    {
        // m_connection = PQconnectdb(conninfo_);
        m_connection.reset( PQconnectdb(conninfo_), &PQfinish );
        // std::cout << "Connection status: " << PQstatus( m_connection.get() ) << std::endl;

        if (PQstatus( m_connection.get() ) != CONNECTION_OK && PQsetnonblocking(m_connection.get(), 1) != 0 )
        {
            std::cout << "Connection error: " << PQstatus( m_connection.get() ) << std::endl;
            throw std::runtime_error( PQerrorMessage( m_connection.get() ) );            
        }
    }
    
    // PGconn* connection() const
    std::shared_ptr<PGconn> connection() const
    {
        return m_connection;
    }

private:
    void establish_connection();

    std::string m_dbhost = "localhost";
    int         m_dbport = 5432;
    std::string m_dbname = "demo";
    std::string m_dbuser = "postgres";
    std::string m_dbpass = "postgres";

    std::shared_ptr<PGconn>  m_connection;
    // PGconn* m_connection;
    char *conninfo;

};


#endif //PGCONNECTION_H

