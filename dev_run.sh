#!/bin/bash
set -e
echo "export CONTAINER_ID=`head -1 /proc/self/cgroup|cut -d/ -f3`" >> /root/.bashrc
source /opt/ros/kinetic/setup.bash
sleep ${START_DELAY_WAIT:-0}
./devel/lib/postgres_ros_twist_publisher/postgres_ros_twist_publisher --container_id=${CONTAINER_ID:-1} --postgres_host_ip=${POSTGRES_HOST_IP:-} --postgres_port=${POSTGRES_PORT:-} --postgres_user=${POSTGRES_USER:-postgres} --postgres_password=${POSTGRES_PASSWORD:-password} --postgres_db_name=${POSTGRES_DB_NAME:-postgres} --tick_rate=${TICK_RATE:-50} --postgres_subscribe_channel_name=${POSTGRES_SUBSCRIBE_CHANNEL_NAME:-mt_l0} --ros_publish_topic_name=${ROS_PUBLISH_TOPIC_NAME:-/X1/cmd_vel} --postgres_subscribe_channel_ids=${POSTGRES_SUBSCRIBE_CHANNEL_IDS:-355034240,2105824522} --postgres_subscribe_channel_ids_to_topic=${POSTGRES_SUBSCRIBE_CHANNEL_IDS_TO_TOPIC:-linear.x,angular.z} --postgres_subscribe_channel_types=${POSTGRES_SUBSCRIBE_CHANNEL_TYPES:-0} --postgres_schemas=${POSTGRES_SCHEMAS:-cognition}