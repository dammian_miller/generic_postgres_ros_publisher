/*
* Copyright (c) 2008, Willow Garage, Inc.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the Willow Garage, Inc. nor the names of its
*       contributors may be used to endorse or promote products derived from
*       this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

/*
* Copyright (c) 2020 Miller-Tech VIC Pty. Ltd.
* All rights reserved.
* Adapted (poorly) from Willow Garage's *excellent* work at: 
* http://youngsf.cn/indigo/api/tf2_ros/html/c++/transform__listener_8cpp_source.html
*/

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <chrono>
#include <ros/ros.h>
#include <getopt.h>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <netinet/in.h>
#include <city.h>
#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"
#include "boost/thread.hpp"
#include "std_msgs/Empty.h"
#include "geometry_msgs/Transform.h"
#include <geometry_msgs/Twist.h>
#include <rosgraph_msgs/Clock.h>

using namespace std::chrono;

int16_t UnpackA(int32_t x)
{
   return (int16_t)(((uint32_t)x)>>16);
}

int16_t UnpackB(int32_t x)
{
   return (int16_t)(((uint32_t)x)&0xffff);
}

uint32_t high(uint64_t combined)
{
    return combined >> 32;
}

uint32_t low(uint64_t combined)
{
    uint64_t mask = std::numeric_limits<uint32_t>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

int main(int argc, char **argv)
{
	int tick_rate = 10;
	int thread_count = 1;
	int opt = 0;
	std::string postgres_host_ip = "";
	std::string postgres_port = "5432";
	std::string xmlrpc_port = "12801";
	std::string tcpros_port = "12802";
	std::string postgres_db_name = "postgres";
	std::string postgres_user = "";
	std::string postgres_password = "";
	std::string postgres_schemas = "public";
	std::string postgres_subscribe_channel_name = "";
	std::string ros_publish_topic_name = "";
	std::stringstream postgres_subscribe_channel_ids;
	std::stringstream postgres_subscribe_channel_types;
	std::stringstream postgres_subscribe_channel_ids_to_topic;
	std::vector<std::string> postgres_subscribe_channel_ids_list;
	std::vector<std::string > postgres_subscribe_channel_ids_to_topic_list;
	std::string container_id = "postgres_ros_cmd_vel";
	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"postgres_host_ip", 1, 0, 'a'},
		{"postgres_port", 1, 0, 'b'},
		{"postgres_db_name", 1, 0, 'c'},
		{"postgres_user", 1, 0, 'd'},
		{"postgres_password", 1, 0, 'e'},
		{"container_id", 1, 0, 'f'},
		{"postgres_subscribe_channel_name", 1, 0, 'g'},
		{"ros_publish_topic_name", 1, 0, 'h'},
		{"postgres_subscribe_channel_ids", 1, 0, 'i'},
		{"postgres_subscribe_channel_ids_to_topic", 1, 0, 'j'},
		{"tick_rate", 1, 0, 'k'},
		{"postgres_subscribe_channel_types", 1, 0, 'l'},
		{"postgres_schemas", 1, 0, 'm'},
		{"xmlrpc_port", 1, 0, 'n'},
		{"tcpros_port", 1, 0, 'o'},

		
		{0, 0, 0, 0}};

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case 'a':
			std::cout << "setting postgres_host_ip: " << optarg << std::endl;
			postgres_host_ip = optarg;
			break;
		case 'b':
			std::cout << "setting postgres_port: " << optarg << std::endl;
			postgres_port = optarg;
			break;
		case 'c':
			std::cout << "setting postgres_db_name: " << optarg << std::endl;
			postgres_db_name = optarg;
			break;
		case 'd':
			std::cout << "setting postgres_user: " << optarg << std::endl;
			postgres_user = optarg;
			break;
		case 'e':
			std::cout << "setting postgres_password: " << optarg << std::endl;
			postgres_password = optarg;
			break;
		case 'f':
			std::cout << "setting container_id: " << optarg << std::endl;
			container_id = optarg;
			break;
		case 'g':
			std::cout << "setting postgres_subscribe_channel_name: " << optarg << std::endl;
			postgres_subscribe_channel_name = optarg;
			break;
		case 'h':
			std::cout << "setting ros_publish_topic_name: " << optarg << std::endl;
			ros_publish_topic_name = optarg;
			break;
		case 'i':
			std::cout << "setting postgres_subscribe_channel_ids: " << optarg << std::endl;
			postgres_subscribe_channel_ids << optarg;
			while( postgres_subscribe_channel_ids.good() )
			{
				std::string substr;
				getline( postgres_subscribe_channel_ids, substr, ',' );
				if(substr.length() != 0)
				{
					postgres_subscribe_channel_ids_list.push_back( substr );
				}
			}
			break;
		case 'j':
			std::cout << "setting postgres_subscribe_channel_ids_to_topic: " << optarg << std::endl;
			postgres_subscribe_channel_ids_to_topic << optarg;
			while( postgres_subscribe_channel_ids_to_topic.good() )
			{
				std::string substr;
				getline( postgres_subscribe_channel_ids_to_topic, substr, ',' );
				if(substr.length() != 0)
				{
					postgres_subscribe_channel_ids_to_topic_list.push_back( substr );
				}
			}
			break;
		case 'k':
			std::cout << "setting tick_rate: " << optarg << std::endl;
			tick_rate = atoi(optarg);
			break;
		case 'l':
			std::cout << "setting postgres_subscribe_channel_types: " << optarg << std::endl;
			postgres_subscribe_channel_types << optarg;
			break;
		case 'm':
			std::cout << "setting postgres_schemas: " << optarg << std::endl;
			postgres_schemas = optarg;
			break;		
		case 'n':
			std::cout << "setting xmlrpc: " << optarg << std::endl;
			xmlrpc_port = optarg;
			break;		
		case 'o':
			std::cout << "setting tcpros: " << optarg << std::endl;
			tcpros_port = optarg;
			break;		
		default:
			exit(EXIT_FAILURE);
		}
	}

	

	std::cout << "Starting..." << std::endl;

	if(postgres_subscribe_channel_name.length() == 0)
	{
		std::cout << "No Postgres channels set to listen/subscribe to. Exiting..." << std::endl;;
		exit(1);
	}
	if(ros_publish_topic_name.length() == 0)
	{
		std::cout << "No ROS topics set for publishing. Exiting..." << std::endl;;
		exit(1);
	}
	if(postgres_subscribe_channel_ids_list.size() == 0)
	{
		std::cout << "No Postgres channel IDs set to process. Exiting..." << std::endl;;
		exit(1);
	}
	postgres_subscribe_channel_types.seekp(0, std::ios::end);
	std::stringstream::pos_type offset = postgres_subscribe_channel_types.tellp();
	postgres_subscribe_channel_types.seekp(0, std::ios::beg);
	if(offset == 0)
	{
		std::cout << "No Postgres channel types set to process. Exiting..." << std::endl;;
		exit(1);
	}
	if(postgres_subscribe_channel_ids_to_topic_list.size() == 0)
	{
		std::cout << "No IDs to topic mapping set. Exiting..." << std::endl;;
		exit(1);
	}
	if(postgres_subscribe_channel_ids_to_topic_list.size() != postgres_subscribe_channel_ids_list.size())
	{
		std::cout << "Count of IDs must match Topics map count. Exiting..." << std::endl;;
		exit(1);
	}

	std::map<std::string, std::string> id_attribute_map;
	for (int i = 0, l = postgres_subscribe_channel_ids_list.size(); i < l; i++)
	{
		id_attribute_map[postgres_subscribe_channel_ids_list.at(i)] = postgres_subscribe_channel_ids_to_topic_list.at(i);
	}

	std::string ros_node_name = "postgres_ros_" + container_id;

	ros::init(argc, argv, ros_node_name);
	ros::NodeHandle nh;
	
	nh.setParam("xmlrpc_port",xmlrpc_port);
	nh.setParam("tcpros_port",tcpros_port);

	ros::master::V_TopicInfo topic_infos;
	ros::master::getTopics(topic_infos);
	std::cout << "ROS topics: " << std::endl;
	for (int i, l = topic_infos.size(); i < l; i++)
	{
		std::cout << "topic name: " << topic_infos[i].name << " topic type: " << topic_infos[i].datatype << std::endl;
	}
	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgres_db_name + "' user='" + postgres_user + "' password='" + postgres_password + "' options='-c search_path=" + postgres_schemas + "'";
	// only if host IP specified then switch to TCP
	if(postgres_host_ip.length() > 0)
	{
		db_conn_str = "postgresql://" + postgres_user + ":" + postgres_password + "@" + postgres_host_ip + ":" + postgres_port + "/" + postgres_db_name + "?options=-c%20search_path%3D" + postgres_schemas;
	}
	std::cout << db_conn_str << std::endl;
	char conninfo[db_conn_str.size() + 1];
	strcpy(conninfo, db_conn_str.c_str());

	std::shared_ptr<PGBackend> pgbackend;
	pgbackend = std::make_shared<PGBackend>(db_conn_str, thread_count);
	std::shared_ptr<PGConnection> conn = pgbackend->connection();
	PGresult *res = NULL;
	PGnotify *notify;
	int nnotifies;

	// upsert publish channel and filter IDs to publish
	std::string sql = ("insert into mt_pub(tp,t,i) values ('" + postgres_subscribe_channel_name + "','" + postgres_subscribe_channel_types.str() + "','" + postgres_subscribe_channel_ids.str() + "') on conflict (tp,t,i) do nothing");
	res = PQexec(conn->connection().get(), sql.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		fprintf(stderr, "Upsert listen channel name and IDs to filter on command failed: %s", PQerrorMessage(conn->connection().get()));
	}

	// should PQclear PGresult whenever it is no longer needed to avoid memory leaks
	PQclear(res);

	// Listen/subscribe to channel
	sql = ("LISTEN " + postgres_subscribe_channel_name);
	res = PQexec(conn->connection().get(), sql.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		fprintf(stderr, "LISTEN command failed: %s", PQerrorMessage(conn->connection().get()));
		PQclear(res);
		pgbackend->freeConnection(conn);
	}

	// should PQclear PGresult whenever it is no longer needed to avoid memory leaks
	PQclear(res);
	
	std::cout <<  "publishing to topic: " << ros_publish_topic_name << std::endl;
	bool latchOn = 1;
	ros::Publisher publisher = nh.advertise<geometry_msgs::Twist>(ros_publish_topic_name, 1000, latchOn);

	while (true)
	{
		/*
         * Sleep until something happens on the connection.  We use select(2)
         * to wait for input, but you could also use poll() or similar
         * facilities.
         */
		int sock;
		fd_set input_mask;

		sock = PQsocket(conn->connection().get());

		if (sock < 0)
			break; /* shouldn't happen */

		FD_ZERO(&input_mask);
		FD_SET(sock, &input_mask);

		if (select(sock + 1, &input_mask, NULL, NULL, NULL) < 0)
		{
			fprintf(stderr, "select() failed: %s\n", strerror(errno));
			pgbackend->freeConnection(conn);
			exit(1);
		}

		/* Now check for input */
		PQconsumeInput(conn->connection().get());
		while ((notify = PQnotifies(conn->connection().get())) != NULL)
		{
			geometry_msgs::Twist msg;
			std::string id;
			std::string val;
			std::stringstream payload;
			payload << notify->extra;
			if( payload.good() )
			{
				std::string substr;
				getline( payload, substr, ',' );
				if(substr.length() != 0)
				{
					id = substr;
				}
				getline( payload, substr, ',' );
				if(substr.length() != 0)
				{
					val = substr;
				}
				std::cout << "id: " << id << " val: " << std::to_string(std::stod(val)/1000000) << std::endl;

				for(std::vector<std::string>::size_type i = 0; i != postgres_subscribe_channel_ids_list.size(); i++) 
				{
    				if(postgres_subscribe_channel_ids_list[i].compare(id) == 0)
					{
						substr = postgres_subscribe_channel_ids_to_topic_list[i];
					}
				}
 
				if(substr.compare("linear.x") == 0)
				{
					msg.linear.x = std::stod(val)/1000000;
				}
				else if(substr.compare("linear.y") == 0)
				{
					msg.linear.y = std::stod(val)/1000000;
				}
				else if(substr.compare("linear.z") == 0)
				{
					msg.linear.z = std::stod(val)/1000000;
				}
				else if(substr.compare("angular.x") == 0)
				{
					msg.angular.x = std::stod(val)/1000000;
				}
				else if(substr.compare("angular.y") == 0)
				{
					msg.angular.y = std::stod(val)/1000000;
				}
				else if(substr.compare("angular.z") == 0)
				{
					msg.angular.z = std::stod(val)/1000000;
				}
				std::cout <<  "publish msg " << std::endl << msg << std::endl;
				publisher.publish(msg);
				ros::spinOnce();
			}
		}
		PQfreemem(notify);
	}
	pgbackend->freeConnection(conn);
	return 0;
}
